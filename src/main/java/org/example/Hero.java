package org.example;

import java.util.ArrayList;

public abstract class Hero {
    private String name;
    private int health=0;
    private int level;
    private int levelAttribtues;

    private int dexterity=0;

    private int Intelligence=0;
    private ArrayList<String> equipment;
    private ArrayList<String> validWeaponTypes;
    private ArrayList<String> validArmorTypes;

    public Hero(String name) {
        equipment=new ArrayList<>();
        validWeaponTypes=new ArrayList<>();
        validArmorTypes=new ArrayList<>();
        this.name = name;
    }

    public abstract void levelUp();

    public abstract void equip(ArrayList equipments);

    public abstract void damage(int damage);

    public abstract int totalAttributes();

    public abstract void display();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevelAttribtues() {
        return levelAttribtues;
    }

    public void setLevelAttribtues(int levelAttribtues) {
        this.levelAttribtues = levelAttribtues;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return Intelligence;
    }

    public void setIntelligence(int intelligence) {
        Intelligence = intelligence;
    }

    public ArrayList<String> getEquipment() {
        return equipment;
    }

    public void setEquipment(ArrayList<String> equipment) {
        this.equipment = equipment;
    }

    public ArrayList<String> getValidWeaponTypes() {
        return validWeaponTypes;
    }

    public void setValidWeaponTypes(ArrayList<String> validWeaponTypes) {
        this.validWeaponTypes = validWeaponTypes;
    }

    public ArrayList<String> getValidArmorTypes() {
        return validArmorTypes;
    }

    public void setValidArmorTypes(ArrayList<String> validArmorTypes) {
        this.validArmorTypes = validArmorTypes;
    }

}
