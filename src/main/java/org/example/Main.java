package org.example;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    Scanner scanner=new Scanner(System.in);

    public static void main(String[] args) {
        Main main=new Main();
        int choose = main.start();
        String name = main.heroName();

        //this check if you enter the right choise
        if (choose < 1 && choose > 4){
            throw new RuntimeException("This is not a valid choose");
        }

        //this code will check witch karakter you want and will make an object of it
        switch (choose){
            case 1:
                HeroMage mage=new HeroMage(name);
                main.mageHero(mage);
                break;
            case 2:
                HeroRanger ranger=new HeroRanger(name);
                main.rangerHero(ranger);
                break;
            case 3:
                HeroRogue rogue=new HeroRogue(name);
                main.rogueHero(rogue);
                break;
            case 4:
                HeroWarrior warrior=new HeroWarrior(name);
                main.warriorHero(warrior);
                break;
        }
    }

    //this code will detarminate hero behavior
    public void mageHero(HeroMage mage){
        mage.dexterity(1);
        mage.intelligence(8);
        mage.strenght(1);
        mage.display();
        System.out.println("\n\nTo level up, you have to equip some atributes\nThese are the equipment you have available [Sord, Shield]\nWould you like to equip them?");
        String choose = scanner.nextLine();


        if (choose.equals("yes")){
            ArrayList equip=new ArrayList();
            equip.add("Sord");
            equip.add("Shield");

            mage.equip(equip);
            mage.dexterity(1);
            mage.intelligence(5);
            mage.strenght(1);
            mage.levelUp();
            mage.display();
        }else {
            mage.display();
        }
    }

    public void rangerHero(HeroRanger ranger){
        ranger.dexterity(7);
        ranger.intelligence(1);
        ranger.display();
        System.out.println("\n\nTo level up, you have to equip some atributes\nThese are the equipment you have available [Sord, Shield]\nWould you like to equip them?");
        String choose = scanner.nextLine();


        if (choose.equals("yes")){
            ArrayList equip=new ArrayList();
            equip.add("Sord");
            equip.add("Shield");

            ranger.equip(equip);
            ranger.dexterity(5);
            ranger.intelligence(1);
            ranger.strenght(1);
            ranger.levelUp();
            ranger.display();
        }else {
            ranger.display();
        }
    }

    public void rogueHero(HeroRogue rogue){
        rogue.dexterity(6);
        rogue.strenght(2);
        rogue.intelligence(1);
        rogue.display();
        System.out.println("\n\nTo level up, you have to equip some atributes\nThese are the equipment you have available [Sord, Shield]\nWould you like to equip them?");
        String choose = scanner.nextLine();

        if (choose.equals("yes")){
            ArrayList equip=new ArrayList();
            equip.add("Sord");
            equip.add("Shield");

            rogue.equip(equip);
            rogue.dexterity(4);
            rogue.intelligence(1);
            rogue.strenght(1);
            rogue.levelUp();
            rogue.display();
        }else {
            rogue.display();
        }
    }

    public void warriorHero(HeroWarrior warrior){
        warrior.dexterity(6);
        warrior.strenght(2);
        warrior.intelligence(1);
        warrior.display();
        System.out.println("\n\nTo level up, you have to equip some atributes\nThese are the equipment you have available [Sord, Shield]\nWould you like to equip them?");
        String choose = scanner.nextLine();


        if (choose.equals("yes")){
            ArrayList equip=new ArrayList();
            equip.add("Sord");
            equip.add("Shield");

            warrior.equip(equip);
            warrior.dexterity(4);
            warrior.intelligence(1);
            warrior.strenght(1);
            warrior.levelUp();
            warrior.display();
        }else {
            warrior.display();
        }
    }

    public int start(){
        System.out.println("Please choose between these type of Hero's");
        System.out.println("Mage -> 1\nRanger -> 2\nRogue -> 3\nWarrior -> 4");
        return scanner.nextInt();
    }

    //this code will get the name you enter for your karakter
    public String heroName(){
        System.out.println("Please choose a name for the Hero");
        return scanner.nextLine();
    }

}
