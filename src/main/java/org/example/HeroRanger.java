package org.example;

import java.util.ArrayList;

public class HeroRanger extends Hero implements HeroAttribute{

    public HeroRanger(String name) {
        super(name);
    }

    //this method will increase hero level by 1
    @Override
    public void levelUp() {
        setLevel(this.getLevel()+1);

    }

    //this method will equip the attributes that the hero has available
    @Override
    public void equip(ArrayList equipments) {
        this.setEquipment(equipments);
    }

    //this method gives demage to hero health
    @Override
    public void damage(int damage) {
        int health=getHealth()-damage;
        this.setHealth(health);
    }

    //this method shows how mane equipment karakter has equipt
    @Override
    public int totalAttributes() {
        return getEquipment().size();
    }

    //this method displays the information about Hero karakter
    @Override
    public void display() {
        System.out.printf("Hero type -> Ranger\nHero name -> %s\nHero Strenght-> %d\nHero equipement - > %s\nHero Dexterity -> %d\nHero intelligence -> %d\n",getName(),getHealth(),getEquipment().size()==0?getEquipment().toString():"0",getDexterity(),getIntelligence());
    }

    // This method increase strenght  by the given number
    @Override
    public void strenght(int strenght) {
        int health=getHealth()+strenght;
        this.setHealth(health);
    }

    // This method increase dexterity  by the given number
    @Override
    public void dexterity(int dex) {
        int dexterity= this.getDexterity()+dex;
        this.setDexterity(dexterity);
    }

    // This method increase intelligence by the given number
    @Override
    public void intelligence(int intell) {
        int intelligence=getIntelligence()+intell;
        this.setIntelligence(intelligence);
    }
}
