package org.example;

public interface HeroAttribute {
    void strenght(int strenght);
    void dexterity(int dex);
    void intelligence(int intell);
}
