import org.example.HeroMage;
import org.example.Main;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class HeroTest {

    HeroMage mage=new HeroMage("koen");

    //This method is to test if the equipment are added properly
    @Test
    public void testEquipAttributes(){
        ArrayList arrayList=new ArrayList();
        arrayList.add("Sord");
        arrayList.add("Shield");
        mage.equip(arrayList);

        Assertions.assertEquals(2,mage.getEquipment().size());
    }
}
